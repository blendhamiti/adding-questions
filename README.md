# Adding questions

Steps to run the script
- Fill in the '.env' file similar to the '.env.example' file
- Insert the correct value for the constant `questionID` in 'locations.tsx' or 'languages.tsx'
- Run `npm run start:locations` or `npm run start:languages`