import * as dotenv from 'dotenv';
import { createConnection } from 'mysql2/promise';
import * as fs from 'fs';
const parse = require('csv-parse/lib/sync');

const main = async () => {
  dotenv.config();

  try {
    const filePath = 'locations.csv';

    const file = fs.readFileSync(filePath);

    const csv = parse(file);
    
    const answers: string[] = csv
      .map((row: any[]) => row[2])
      .slice(1)
      .map((answer: string) => answer.replace(/,/g, ', '));

    console.log(answers);
    
    const conn = await createConnection({
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      user: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
    });

    const questionID = 16;

    const answerStmts = answers.map(answer => `(${questionID}, "${answer}")`);

    const bulkAnswers = [];
    const size = 1000;
    let curr = 0;
    while (curr < answerStmts.length) {
      bulkAnswers.push(answerStmts.slice(curr, curr + size));
      curr += size;
    }
      
    await Promise.all(
      bulkAnswers.map(async (bulk) => {
        try {
          await conn.query(
            `INSERT INTO \`mykeylive\`.\`KeychainTalentPrimaryAnswers\` (\`KeychainTalentQuestionID\`, \`Description\`) VALUES ${bulk.join(',')};`
          );
          
        } catch (error) {
          console.log(error);
        }
      })
    );
  
    await conn.end();  
  } catch (error) {
    console.log(error);
  }
};

main();
