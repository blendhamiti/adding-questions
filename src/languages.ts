import * as dotenv from 'dotenv';
import { createConnection } from 'mysql2/promise';
import * as fs from 'fs';
const parse = require('csv-parse/lib/sync');

const main = async () => {
  dotenv.config();

  try {
    const filePath = 'languages.csv';

    const file = fs.readFileSync(filePath);

    const csv = parse(file);

    const answers: string[] = csv.map((row: any[]) => row[0])

    console.log(answers);
    
    const conn = await createConnection({
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      user: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
    });

    const questionID = 16;

    await Promise.all(
      answers.map(async (answer) => {
        try {
          await conn.query(
            `INSERT INTO \`mykeylive\`.\`KeychainTalentPrimaryAnswers\` (\`KeychainTalentQuestionID\`, \`Description\`) VALUES (${questionID}, "${answer}");`
          );
        } catch (error) {
          console.log(error);
        }
      })
    );
  
    await conn.end();  
  } catch (error) {
    console.log(error);
  }
};

main();
